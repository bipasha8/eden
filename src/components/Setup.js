import { Box } from "@mui/system";
import React, { useState } from "react";
import Header from "./Header";
import { useHistory } from "react-router-dom";
import "./App.css";
import "./Setup.css";


export default function Setup() {


    const [wName, setWName] = useState('');
    const [workspaceUrl, setWorkspaceUrl] = useState('');

    const [wNameError, setWNameError] = useState({});
    const [workspaceUrlError, setWorkspaceUrlError] = useState({});

    const history = useHistory();



    const onSubmit = (e) => {
        e.preventDefault();
        const isValid = formValidation();
        if (isValid) {
            history.push("/usage");
            setWName("");
            setWorkspaceUrl("");
        }
    }


    const formValidation = () => {
        const wNameError = {};
        const workspaceUrlError = {};
        let isValid = true;


        if (!wName.match(/^([a-zA-Z ]){3,15}$/)) {
            wNameError.nameValid = "Enter a valid workspace name";
            isValid = false;
        }

        if (workspaceUrl !== "") {
            if (!workspaceUrl.match(/^"(www.)?"+"[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]"+"{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)"/)) {
                workspaceUrlError.urlValid = "Enter a valid url";
                isValid = false;
            }
        }


        setWNameError(wNameError);
        setWorkspaceUrlError(workspaceUrlError);

        return isValid;
    }





    return (
        <Box className="container-welcome-title">
            <div className="header"><Header /></div>
            <div className="container-body">
                <h2 className="">Let's setup a home for all your work</h2>
                <p style={{ color: '#a9a9a9', fontSize: '16px' }}>You can always create another workspace later.</p>
            </div>
            <form className="form" onSubmit={onSubmit}>
                <label htmlFor="workspaceName" className="label-control">Workspace Name</label><br />
                <input type="text" placeholder="Eden" className="input-control" name="workspaceName" onChange={(e) => { setWName(e.target.value) }} /><br />
                {Object.keys(wNameError).map((key) => {
                    return <div style={{ color: "red" }}>{wNameError[key]}</div>
                })}
                <label htmlFor="workspaceName" className="label-control">Workspace URL
                    <span style={{ color: '#a9a9a9', fontSize: '16px' }}> (Optional)</span>

                </label><br />
                <br />
                <div className="workspace-url">
                    <input className="input-url-sample" disabled placeholder="www.eden.com/" />
                    <input placeholder="Example" type="text" className="input-url" name="exampleurl" onChange={(e) => { setWorkspaceUrl(e.target.value) }} /><br />
                    {Object.keys(workspaceUrlError).map((key) => {
                        return <div style={{ color: "red" }}>{workspaceUrlError[key]}</div>
                    })}
                </div>
                <button type="submit" className="create-btn"  >Create Workspace</button>

            </form>

        </Box>
    )
}