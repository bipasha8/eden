import React from "react";
import {Route, Switch, BrowserRouter} from "react-router-dom";
import Home from "./Home";
import Setup from "./Setup";
import Welcome from "./Welcome";
import "./App.css";
import Usage from "./Usage";
import Thanks from "./Thanks";



export default function App(props){

    
return(
    <div className="App">
        <BrowserRouter>
        <Switch>
            <Route exact path="/">
                <Home />
            </Route>
            <Route path="/welcome">
                <Welcome />
            </Route>
            <Route path="/setup">
                <Setup />
            </Route>
            <Route path="/usage">
                <Usage />
            </Route>
            <Route path="/thanks">
                <Thanks />
            </Route>
        </Switch>
        </BrowserRouter>
    </div>
)
}