import React from "react";
import { Typography,Box } from '@mui/material';
import Eden from "./images/Eden.png";
import './Header.css';

export default function Header(){
    return(
        <Box className="header">  
            <img className="header-img" src={Eden} alt="Eden" />
            <Typography className="header-text">Eden</Typography>
        </Box>
    )
}