import { Box } from "@mui/material";
import React from "react";
import { useHistory } from "react-router-dom";
import screen1 from "../components/images/screen1.png";



export default function Home(props){
    const history = useHistory();

    const redirect = () => {
        history.push('/welcome');
    }
    return(
        <Box className="container" style={{margin:"20px", padding:"20px"}}>
            <img src={screen1} alt="screen1" style={{width:"100%",height:"100%",objectFit:"cover"}}/>
            <button type="submit" className="button" onClick={redirect}>Create Workspace</button>
        </Box>
    )
}