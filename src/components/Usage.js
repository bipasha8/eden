import { Box } from "@mui/system";
import React, { useState } from "react";
import Header from "./Header";
import { useHistory } from "react-router-dom";
import User from "./images/User_icon.png";
import MultipleUser from "./images/MultipleUser_icon.png";
import { CardGroup, Card } from 'react-bootstrap';
import "./App.css";
import "./Usage.css";



const cardStyle = {

    background: "#fff",

    maxWidth: "18rem", border: "solid #889EAF 1px", borderRadius: "10px"
};

const cardSelect = {
    boxShadow: "2px 4px 30px 0px rgba(0, 0, 0, 0.75)",
    borderColor: "Blue"
};

export default function Usage() {

    const [selected, setSelected] = useState(0);

    const history = useHistory();

    const redirect = () => {
        history.push('/thanks');
    }

    return (
        <Box className="container-welcome-title">
            <div className="header"><Header /></div>
            <div className="container-body">
                <h2 className="title">How are you planning to use Eden?</h2>
                <p style={{ color: '#a9a9a9', fontSize: '16px' }}>We,ll streamline your setup experience accordingly</p>
            </div>




            {/* cards */}


            <CardGroup style={{ display: "flex", justifyContent: "space-evenly" }}>
                <Card
                    style={{ ...cardStyle, ...(selected === 0 && cardSelect) }}
                    onClick={() => setSelected(0)}
                // style={{ maxWidth: "18rem", border: "solid #889EAF 1px", borderRadius: "10px" }}

                >
                    <div style={{ margin: "1rem" }}>
                        <img src={User} alt="User" />
                        <h5 style={{ margin: "0.5rem 0" }}>
                            For Myself
                        </h5>
                        <p style={{ color: "#87918a" }}>Write better. Think more clearly. Stay organized.</p>
                    </div>
                </Card>

                <Card
                    style={{ ...cardStyle, ...(selected === 1 && cardSelect) }}
                    onClick={() => setSelected(1)}
                // style={{ maxWidth: "18rem", border: "solid #889EAF 1px", borderRadius: "10px" }}

                >
                    <div style={{ margin: "1rem" }}>
                        <img src={MultipleUser} alt="Multiple Users" />

                        <h5 style={{ margin: "0.5rem 0" }}>
                            With my Team
                        </h5>
                        <p style={{ color: "#87918a" }}>Wikis,docs,tasks & projects, all in one place.</p>
                    </div>
                </Card>
            </CardGroup>
            <div className="usage-styles">
                <button type="submit" className="button" onClick={redirect}>Create Workspace</button>
            </div>
        </Box>
    )
}