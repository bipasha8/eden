import { Box } from "@mui/material";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Header from "./Header";
import "./Welcome.css";
import "./App.css";

export default function Welcome() {

    

    

    const [name, setName] = useState('');
    const [displayName, setDisplayName] = useState('');

    const [nameError, setNameError] = useState({});
    const [displayNameError, setDisplayNameError] = useState({});

    const history = useHistory();

    const onSubmit = (e) => {
        e.preventDefault();
        const isValid = formValidation();
        if (isValid) {
            history.push("/setup")
            setName("");
            setDisplayName("");
        }
    }

    const formValidation = () => {
        const nameError = {};
        const displayNameError = {};
        let isValid = true;

        if (!name.match(/^[[a-zA-Z]+([\s][a-zA-Z]+)*$/)) {
            nameError.nameValid = "Enter a valid full name";
            isValid = false;
        }
        if (!displayName.match(/^(?=.{5,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/)) {
            displayNameError.displayNameValid = " Enter a valid Display name";
            isValid = false;
        }

        setNameError(nameError);
        setDisplayNameError(displayNameError);

        return isValid;
    }
    return (
        <Box className="container" >
            <div className="header"><Header /></div>
            <div className="container-body">
                <h2 className="title">Welcome! First things first...</h2>
                <p style={{ color: '#a9a9a9', fontSize: '16px' }}>You can always change them later.</p>
            </div>
            <form className="form" onSubmit={onSubmit}>
                <label htmlFor="name" className="label-control">Full Name</label><br />
                <input id="fName" type="text" className="input-control" name="fName" placeholder="Steve Jobs" onChange={(e) => { setName(e.target.value) }} /><br />
                {Object.keys(nameError).map((key) => {
                    return <div style={{ color: "red" }}>{nameError[key]}</div>
                })}
                <label htmlFor="phone" className="label-control">Display Name</label><br />
                <input id="dName" type="text" className="input-control" name="dName" placeholder="Steve" onChange={(e) => { setDisplayName(e.target.value) }} /><br />
                {Object.keys(displayNameError).map((key) => {
                    return <div style={{ color: "red" }}>{displayNameError[key]}</div>
                })}
                <button type="submit" className="create-btn"  >Create Workspace</button>
            </form>
        </Box>
    )
}