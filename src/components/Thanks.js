import { Box } from "@mui/system";
import React from "react";
import Header from "./Header";
import "./Thanks.css";


export default function Thanks() {
    return (
        <Box className="container-welcome-title">
            <div className="header"><Header /></div>
            <div className="container-body">
                <h2 className="title">Congratulations!</h2>
                <p style={{ color: '#a9a9a9', fontSize: '16px' }}>We,ll streamline your setup experience accordingly</p>
            </div>
            <div className="button-style">
                <button type="submit" className="create-btn"  >Launch Eden</button>

            </div>
        </Box>
    )
}